% Created 2019-12-22 Sun 13:54
% Intended LaTeX compiler: lualatex
\documentclass[10pt]{article}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{sparklistings}
\usepackage{tcolorbox}
\DeclareMathOperator{\steps}{steps}
\newcommand{\Lower}{\textit{lower}}
\newcommand{\Upper}{\textit{upper}}
\newcommand{\Body}{\textit{body}}
\newcommand{\BoolExpr}{\textit{bool\_expr}}
\newtcolorbox{spasm}{colframe=red!75!black,sidebyside,lefthand width=1.6in}
\lstnewenvironment{sparks}{\lstset{language=sparks}}{}
\lstnewenvironment{asm}{\lstset{language=asm}}{}
\newcommand{\Sparks}{\textsc{Sparks}}
\newcommand{\Asm}{\textsc{Asm}}

\usepackage[bordercolor=white]{todonotes}
\newcommand{\fixme}[1]{%
    \todo[noline,noinline,caption={#1}]{\textsc{fixme}}%
}

\begin{document}

\section{Compiling \Sparks{} to \Asm{}}
In the following code samples, $x,y,z$ are assumed declared boolean,
$i,j,k$ are assumed declared integer, $A$ is a boolean array, and $B$
is an integer array.
\subsection{Simple assignments}
\label{sec:simple}
 The following \Sparks{} statements
translate one-to-one to \Asm{} statements.
\begin{spasm}
\begin{sparks}
z <- x
z <- !x
z <- x and y
z <- x or y
z <- x xor y
z <- x eq y
z <- i = j
z <- i < j
i <- j
i <- j + k
i <- inc(j)
i <- dec(j)
i++
A[*] <- 0
B[*,*] <- 0
A[i] <- x
B[[i]] <- j
\end{sparks}
  \tcblower
\begin{asm}
. set z copy x
. set z not x
. set z and x y
. set z or x y
. set z xor x y
. set z eq x y
. set z eqw i j
. set z ltw i j
. set i copyw j
. set i addw j k
. set i incw j
. set i decw j
. set i incw i
. array_init A 0
. matrix_init B 0
. array_set A i x
. row_set B i j
\end{asm}
\end{spasm}

\subsubsection{Negated operators}
Currently there is only one negated operator supported. It translates
to two \Asm{} statements. \fixme{Support more negated operators and/or negated parenthesized expressions}
\begin{spasm}
\begin{sparks}
z <- i != j
\end{sparks}
  \tcblower
\begin{asm}
. set _tmp1 eqw i j
. set z not _tmp1
\end{asm}
\end{spasm}

\subsection{Array reads}
Array reads translate to one \Asm{} statement per array reference compared to the statements in Section~\ref{sec:simple}.\fixme{Optimize simple array reads}
\begin{equation*}
  \steps(\text{array using expr}) = \steps(\text{basic expr}) + \#\text{array refs}.
\end{equation*}

\begin{spasm}
\begin{sparks}
x <- A[i]
\end{sparks}
  \tcblower
\begin{asm}
. set  _tmp1 array_ref A i
. set x copy _tmp1
\end{asm}
\end{spasm}

\begin{spasm}
\begin{sparks}
x <- A[i] and A[j]
\end{sparks}
  \tcblower
\begin{asm}
. set  _tmp3 array_ref A i
. set  _tmp4 array_ref A j
. set x and _tmp3 _tmp4
\end{asm}
\end{spasm}

\begin{spasm}
\begin{sparks}
j <- B[[i]]
\end{sparks}
  \tcblower
\begin{asm}
. set  _tmp6 row_ref B i
. set j copyw _tmp6
\end{asm}
\end{spasm}

\begin{spasm}
\begin{sparks}
j <- B[[i]] + B[[k]]
\end{sparks}
  \tcblower
\begin{asm}
. set  _tmp8 row_ref B i
. set  _tmp9 row_ref B k
. set j addw _tmp8 _tmp9
\end{asm}
\end{spasm}

\subsection{Compound assignments}

For convenience \Sparks{} supports a single level of compound
expressions as right-hand-sides. In particular any right hand side
from Subsection~\ref{sec:simple} can be joined by an operator to any
other with matching type (i.e. \texttt{bool} or \texttt{int}).

\begin{equation*}
  \steps(compound)=\steps(rhs) + \steps(lhs) + 1
\end{equation*}

\begin{spasm}
\begin{sparks}
i <- i + j + k
\end{sparks}
  \tcblower
\begin{asm}
. set _tmp1 addw i j
. set _tmp2 copyw k
. set i addw _tmp1 _tmp2
\end{asm}
\end{spasm}
\begin{spasm}
\begin{sparks}
i <- i + j + k + j
\end{sparks}
  \tcblower
\begin{asm}
. set _tmp4 addw i j
. set _tmp5 addw k j
. set i addw _tmp4 _tmp5
\end{asm}
\end{spasm}
\begin{spasm}
\begin{sparks}
z <-(x and y) 
      or (x and z)
\end{sparks}
  \tcblower
\begin{asm}
. set _tmp7 and x y
. set _tmp8 and x z
. set z or _tmp7 _tmp8
\end{asm}
\end{spasm}

\begin{spasm}
\begin{sparks}
z<-(A[0] and A[1]) 
   or (A[2] and A[3])
\end{sparks}
\tcblower
\begin{asm}
. set _tmp12 array_ref A 0
. set _tmp13 array_ref A 1
. set _tmp10 and _tmp12 _tmp13
. set _tmp14 array_ref A 2
. set _tmp15 array_ref A 3
. set _tmp11 and _tmp14 _tmp15
. set z or _tmp10 _tmp11
\end{asm}
\end{spasm}


\subsection{\texttt{if} blocks}
\label{sec:orgcedd1e2}
\begin{spasm}
\begin{sparks}
if `\BoolExpr` then
  `\Body`
endif
\end{sparks}
  \tcblower
\begin{asm}
. set guard0 `\BoolExpr`
. unless guard0 else0
  `\Body`
else0 `\ldots`
\end{asm}
\end{spasm}

\begin{equation*}
  \steps(\mathbf{if}) \leq \steps(\Body) + \steps(\BoolExpr) + 2
\end{equation*}

\subsection{\texttt{if/else} blocks}
\label{sec:orgcedd1e2}
\begin{spasm}
\begin{sparks}
if `\BoolExpr` then
  `$\Body_1$`
else
  `$\Body_2$`
endif
\end{sparks}
  \tcblower
\begin{asm}
. set guard0 `\BoolExpr`
. unless guard0 else0
  `$\Body_1$`
. goto done0
else0 `$\Body_2$`
done0 `\ldots`
\end{asm}
\end{spasm}

\begin{equation*}
  \steps(\mathbf{if-else}) \leq \max_i\steps(\Body_i) + \steps(\BoolExpr) + 3
\end{equation*}

\subsection{\texttt{for} loops}
\label{sec:org6801827}

\label{sec:org98fd16c}

\begin{spasm}
\begin{sparks}
for i<-`lower`,`upper` do
    `body`
done
\end{sparks}
\tcblower
\begin{asm}
. set i `lower`
. set _stop0 `upper`
for0 `body`
.    set _test0 eqw i _stop0
.    if _test0 done0
.    set i incw i
.    goto for0
done0 `\dots`
\end{asm}
\end{spasm}


\begin{equation*}
  \steps(\mathbf{for}) = \steps(\Lower) + \steps(\Upper)
  + \sum_{i=\Lower}^{\Upper}\left(\steps(\Body;i)+4\right)-2
\end{equation*}


\subsection{\texttt{while} loops}

\begin{spasm}
\begin{sparks}
while `\BoolExpr`
      `\Body`
done
\end{sparks}
  \tcblower
\begin{asm}
while1 set _tmp2 `\BoolExpr`
.   set _test1 not _tmp2
.   unless _test1 done1
.   `\Body`
.   goto while1
done1 `\ldots`
\end{asm}
\end{spasm}

\begin{multline*}
  \steps(\mathbf{while}) =   \sum_{i \in \mathrm{iterations}}\left(\steps(\BoolExpr)+\steps(\Body;i)+3\right) \\
  +  \steps(\BoolExpr)+2\\
\end{multline*}

\listoftodos
\end{document}
