The stage1 compiler is written in racket. To run, you need

- The language racket (http://download.racket-lang.org); this is available for
  linux, mac, and windows.

- the parser generator ragg; after installing racket you should have a command
  "raco" as well as "racket". You can run

  % raco pkg install ragg 

  and say yes a few times to install dependencies.

I've only tested with racket 5.3.6; I don't know any reason why later versions wouldn't work also, but you never know.


