\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\bibliographystyle{ieeetr}
\title{Analysis of the makespan code {\em ms.spk}}
\date{2020-05-06}
\usepackage{fullpage}
\usepackage{sparklistings}
\usepackage{tcolorbox}
\usepackage{subcaption}
\usepackage{etoolbox}
\newcommand{\Sparks}{\textsc{Sparks}}
\newcommand{\Asm}{\textsc{Asm}}
\newcommand{\Sdot}[2]{\filldraw(#1,#2) node[rectangle,inner sep=2.5pt,stroke=black,fill=black!75]{};}
\begin{document}
\maketitle
\lstset{language=sparks,frame=single}

\section{Introduction}
The makespan problem is to schedule $m$ jobs on $n$ identical machines to minimize the finishing
time, known as the makespan, of the set of jobs. 

\begin{verbatim}
Instance: integers m,n,and boolean p[0],...,p[m-1].

Problem: Schedule m jobs on n machines to minimize the latest finish 
         time (makespan) T. The time of job i is p[i]+1.

Output:  a job schedule x[m,n]: x[i,j]=1 if job i is scheduled on 
         machine j and zero otherwise.
         return TRUE if termination occurred.
\end{verbatim}

This problem has
exponential extension complexity even for $T=2$, see Tiwary et al.., \cite{TVW19}.
Since the job times are in \{ 1,2 \} a list decreasing schedule is optimum.
This is achieved by the code {\em ms.spk} given in Figure~\ref{sparks}.
The corresponding assembly code {\em ms.asm} is given in Appendix \ref{asmcode}.

The code requires $O(nm)$ space to hold the schedule $x$ and $O(m)$ time as it consists of
two unnested for loops each run $m$ times. 
It follows from results in \cite{ABTW} that the sparktope produced has complexity $O(nm^2)$. 
However to actually produce polytopes for this problem we need to bound,
for each $n, m$, 
the maximum number of assembler steps taken for any instance of this size.
We do this in the next section.

\begin{figure}
  \input{ms-labels.tex}
  \lstinputlisting[style=sparkcount,numbersep=-1.75in]{ms-minimal.spk}
  \begin{tikzpicture}[remember picture,overlay,out=0,in=0,auto]
    \draw (line-7) to node[near start] {$m$ times} (line-14);
    \draw (line-20) to node[near start] {$m$ times} (line-31);
  \end{tikzpicture}

\caption{\Sparks{} source code {\em ms.spk} }
  \label{sparks}
\end{figure}


\section{Step count analysis}

For a given fixed pair of integers $n, m$ we will give an upperbound
on the number of steps taken by {\em ms.asm} in order to reach a
return statement. For this discusion we will refer to
Figure~\ref{sparks}, where the range of assembly lines corresponding to
each \Sparks{} line is given. For more details of the expansion, the reader is referred 
Appendix~\ref{asmcode} and \cite{AB20}.

The first 9 lines are variable declarations and are not executed, so
there are 53 lines of executable \Asm{} code.  We see there are two
unnested for loops each executed $m$ times.  The first has 19 lines
(lines 15-33) and the second has 23 lines (lines 39-61).  So an upper
bound on number of steps executed is $53+42(m-1)=42m+11$. However
inspecting the two for loops we see that they have mutually exclusive
if statements depending on the value of the input $p[i]$.  The body of
these if statements are contained between lines 18-28 and 44-55,
unnested \texttt{for} loops each executed $m$ times.  The first has 19 lines
(lines 15-33) and the second has 21 lines (lines 41-61).  So an upper
bound on number of steps executed is $53+40(m-1)=40m+13$. However
inspecting the two \texttt{for} loops we see that they have mutually exclusive
\texttt{if} statements depending on the value of the input $p[i]$.  The body of
these \texttt{if} statements are contained between lines 18-28 and 44-55,
respectively, and only one of these blocks can be executed for each
$i$. The shorter first block contains 11 statements so we may reduce
the overall running time by $11m$ obtaining the upper bound $29m+13$
for the number of assembler steps executed.  A slightly tighter
analysis is possible by observing that some of the assembly statements
for a \texttt{for} loop are executed only once.

\section{Examples}


Example inputs \texttt{ms5.in}, and \texttt{ms10.in} are provided for
the cases $n=3$ and $m=5, 10$, respectively.  The processor times are
either 1 or 2 so the input for \texttt{ms.spk} can be given by
specifying a boolean array \texttt{p} where the processing time of job
$i$ is $p[i]+1,~i=1,2,...,10$. The \Sparks{} inputs for these examples
are given in Figure~\ref{fig:sample}.

\begin{figure}[!ht]
  \begin{center}
    \begin{tabular}{l|l}
      \texttt{ms5.in} & \lstinline|array p[5] <- {0, 1, 1, 0, 0}|\\
      \texttt{ms10.in}& \lstinline|array p[10] <- {0, 1, 1, 0, 0, 1, 1, 0, 0, 0}|
  \end{tabular}
  \end{center}
  \caption{Sample inputs of 5 and 10 processing times}
  \label{fig:sample}
\end{figure}

The example for $m=5$ takes less than $135$ steps to complete; a trace
at the level of \Sparks{} source lines is shown in
Figure~\ref{fig:ms5}. In the remainder of this section we take a more
detailed look at the \texttt{ms10} example.

The list decreasing algorithm first schedules
the jobs with processing time 2, as shown on the left in
Figure~\ref{fig:schedule}.  The optimal schedule is shown on the right
and has makespan $T=5$.  Figure~\ref{fig:ms10} shows the whole trace,
i.e.\ the whole $S$ matrix, for the run.  We see that the run
terminated at around step 245 at the return statement on line 62 of
the assembler code.  This termination time compares with the upper
bound of $29*10+13=303$ steps calculated above.  In the trace it can
clearly be seen how at around $t=125$ the code switches from
scheduling the jobs with processing time 2 to those with time 1.

\begin{figure}[!ht]
\newcommand{\job}[4]{\draw (#1,#2) node [draw,fill=gray!25,minimum width=#3cm,minimum height=1cm,rounded corners,anchor=south west]  {#4};}

\newcommand{\mlabels}{
    \draw (-0.5,1.5) node[rotate=90,anchor=south] {machine};
    \draw (0,1) node[anchor=south east,minimum height=1cm] {1};
    \draw (0,2) node[anchor=south east,minimum height=1cm] {2};
    \draw (0,3) node[anchor=south east,minimum height=1cm] {3};
}
\newcommand{\tlabels}{
    \draw (2,3) node[anchor=north] {2};
    \draw (4,3) node[anchor=north] {4};
    \draw (6,3) node[anchor=north] {6};
    \draw (3,3.3) node[anchor=north] {$T$};
}
  \begin{subfigure}{0.5\textwidth}
    \caption{Timestep $125$}
    \begin{tikzpicture}[yscale=-1]
      \draw[gray,very thin](0,0) grid  (6,3);
      \job{0}{1}{2}{2}
      \job{0}{2}{2}{3}
      \job{0}{3}{2}{6}
      \job{2}{1}{2}{7}
      \mlabels
      \tlabels
    \end{tikzpicture}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.5\textwidth}
    \caption{Timestep $250$}
    \begin{tikzpicture}[yscale=-1]
      \draw[gray,very thin](0,0) grid  (6,3);
      \job{0}{1}{2}{2}
      \job{0}{2}{2}{3}
      \job{0}{3}{2}{6}
      \job{2}{1}{2}{7}
      \job{2}{2}{1}{1}
      \job{3}{2}{1}{4}
      \job{2}{3}{1}{5}
      \job{3}{3}{1}{8}
      \job{4}{1}{1}{9}
      \job{4}{2}{1}{10}
      \mlabels
      \tlabels
    \end{tikzpicture}
  \end{subfigure}
  \caption{Greedy construction of schedule from input \texttt{ms10.in}}
  \label{fig:schedule}
\end{figure}  


\begin{figure}
  \caption{Trace of the makespan example with $3$ machines and $5$ jobs (\texttt{ms5.in}.}
  \label{fig:ms5}
\begin{tcolorbox}[colframe=black,lefthand width=2.8in,sidebyside,sidebyside align=bottom]
\begin{lstlisting}[language=sparks,frame=none,basicstyle=\small,xleftmargin=0.2in]
input array p[$m$]
matrix x[$m$,$n$]
output bool w
int i; int j; int T; int last
int proc; bool single

x[*,*]<-0; T<-0; proc<-$n-1$

for i<- 0,$m-1$ do
    if p[i] then 
       proc++
       if proc = $n$ then
          proc<-0  
       endif
       x[i,proc]<-1     
       if proc = 0 then
          T <- T + 2
       endif
     endif
done

single<-0
last<-inc(proc)
if last = $n$ then
   last <- 0
endif

for i<- 0,$m-1$ do
    if !p[i] then
       proc++       
       if proc = $n$ then
          if single then
             last<-0
          endif
          proc<-last
          single<-1
          if last = 0 then
             T++
          endif
       endif
       x[i,proc]<-1     
     endif
done

return w@0
\end{lstlisting}
\tcblower  
\begin{tikzpicture}[yscale=-0.43,xscale=0.051]
  \draw[xstep=15,gray,very thin] (0,1) grid (135,45);
  \node at (1,-0.35) {$t$};
  \node at (15,-0.35) {15};
  \node at (30,-0.35) {30};
  \node at (45,-0.35) {45};
  \node at (60,-0.35) {60};
  \node at (75,-0.35) {75};
  \node at (90,-0.35) {90};
  \node at (105,-0.35) {105};
  \node at (120,-0.35) {120};
  \node at (135,-0.35) {135};
\Sdot{1}{7}
\Sdot{2}{7}
\Sdot{3}{7}
\Sdot{4}{9}
\Sdot{5}{9}
\Sdot{6}{10}
\Sdot{7}{10}
\Sdot{8}{10}
\Sdot{9}{10}
\Sdot{10}{9}
\Sdot{11}{9}
\Sdot{12}{9}
\Sdot{13}{9}
\Sdot{14}{10}
\Sdot{15}{10}
\Sdot{16}{10}
\Sdot{17}{11}
\Sdot{18}{12}
\Sdot{19}{12}
\Sdot{20}{13}
\Sdot{21}{15}
\Sdot{22}{16}
\Sdot{23}{16}
\Sdot{24}{17}
\Sdot{25}{17}
\Sdot{26}{17}
\Sdot{27}{16}
\Sdot{28}{10}
\Sdot{29}{9}
\Sdot{30}{9}
\Sdot{31}{9}
\Sdot{32}{9}
\Sdot{33}{10}
\Sdot{34}{10}
\Sdot{35}{10}
\Sdot{36}{11}
\Sdot{37}{12}
\Sdot{38}{12}
\Sdot{39}{15}
\Sdot{40}{16}
\Sdot{41}{16}
\Sdot{42}{16}
\Sdot{43}{10}
\Sdot{44}{9}
\Sdot{45}{9}
\Sdot{46}{9}
\Sdot{47}{9}
\Sdot{48}{10}
\Sdot{49}{10}
\Sdot{50}{10}
\Sdot{51}{10}
\Sdot{52}{9}
\Sdot{53}{9}
\Sdot{54}{9}
\Sdot{55}{9}
\Sdot{56}{10}
\Sdot{57}{10}
\Sdot{58}{10}
\Sdot{59}{10}
\Sdot{60}{9}
\Sdot{61}{9}
\Sdot{62}{22}
\Sdot{63}{23}
\Sdot{64}{24}
\Sdot{65}{24}
\Sdot{66}{28}
\Sdot{67}{28}
\Sdot{68}{29}
\Sdot{69}{29}
\Sdot{70}{29}
\Sdot{71}{30}
\Sdot{72}{31}
\Sdot{73}{31}
\Sdot{74}{41}
\Sdot{75}{29}
\Sdot{76}{28}
\Sdot{77}{28}
\Sdot{78}{28}
\Sdot{79}{28}
\Sdot{80}{29}
\Sdot{81}{29}
\Sdot{82}{29}
\Sdot{83}{29}
\Sdot{84}{28}
\Sdot{85}{28}
\Sdot{86}{28}
\Sdot{87}{28}
\Sdot{88}{29}
\Sdot{89}{29}
\Sdot{90}{29}
\Sdot{91}{29}
\Sdot{92}{28}
\Sdot{93}{28}
\Sdot{94}{28}
\Sdot{95}{28}
\Sdot{96}{29}
\Sdot{97}{29}
\Sdot{98}{29}
\Sdot{99}{30}
\Sdot{100}{31}
\Sdot{101}{31}
\Sdot{102}{32}
\Sdot{103}{32}
\Sdot{104}{35}
\Sdot{105}{36}
\Sdot{106}{37}
\Sdot{107}{37}
\Sdot{108}{37}
\Sdot{109}{41}
\Sdot{110}{29}
\Sdot{111}{28}
\Sdot{112}{28}
\Sdot{113}{28}
\Sdot{114}{28}
\Sdot{115}{29}
\Sdot{116}{29}
\Sdot{117}{29}
\Sdot{118}{30}
\Sdot{119}{31}
\Sdot{120}{31}
\Sdot{121}{32}
\Sdot{122}{32}
\Sdot{123}{33}
\Sdot{124}{35}
\Sdot{125}{36}
\Sdot{126}{37}
\Sdot{127}{37}
\Sdot{128}{38}
\Sdot{129}{37}
\Sdot{130}{41}
\Sdot{131}{29}
\Sdot{132}{28}
\Sdot{133}{28}
\Sdot{134}{45}
\Sdot{135}{45}

\end{tikzpicture}
  
\end{tcolorbox}
\end{figure}

\begin{figure}
  \begin{center}
    \begin{tikzpicture}[yscale=-0.3,xscale=0.051]
  \node at (25,9) {25};
  \node at (50,9) {50};
  \node at (75,9) {75};
  \node at (100,9) {100};
  \node at (125,9) {125};
  \node at (150,9) {150};
  \node at (175,9) {175};
  \node at (200,9) {200};
  \node at (225,9) {225};
  \node at (250,9) {250};

  \node at (-8,8) {$l\backslash{}t$};
  \node at (-7,10) {10};
  \node at (-7,13) {13};
  \node at (-7,24) {24};
  \node at (-7,34) {34};
  \node at (-7,39) {39};
  \node at (-7,61) {61};
  \draw[xstep=25,gray,very thin] (0,10) grid (250,62);

  \Sdot{1}{10} \Sdot{2}{11} \Sdot{3}{12} \Sdot{4}{13} \Sdot{5}{14}
  \Sdot{6}{15} \Sdot{7}{16} \Sdot{8}{17} \Sdot{9}{29} \Sdot{10}{30}
  \Sdot{11}{31} \Sdot{12}{32} \Sdot{13}{33} \Sdot{14}{15}
  \Sdot{15}{16} \Sdot{16}{17} \Sdot{17}{18} \Sdot{18}{19}
  \Sdot{19}{20} \Sdot{20}{21} \Sdot{21}{22} \Sdot{22}{23}
  \Sdot{23}{24} \Sdot{24}{25} \Sdot{25}{26} \Sdot{26}{27}
  \Sdot{27}{28} \Sdot{28}{29} \Sdot{29}{30} \Sdot{30}{31}
  \Sdot{31}{32} \Sdot{32}{33} \Sdot{33}{15} \Sdot{34}{16}
  \Sdot{35}{17} \Sdot{36}{18} \Sdot{37}{19} \Sdot{38}{20}
  \Sdot{39}{22} \Sdot{40}{23} \Sdot{41}{24} \Sdot{42}{28}
  \Sdot{43}{29} \Sdot{44}{30} \Sdot{45}{31} \Sdot{46}{32}
  \Sdot{47}{33} \Sdot{48}{15} \Sdot{49}{16} \Sdot{50}{17}
  \Sdot{51}{29} \Sdot{52}{30} \Sdot{53}{31} \Sdot{54}{32}
  \Sdot{55}{33} \Sdot{56}{15} \Sdot{57}{16} \Sdot{58}{17}
  \Sdot{59}{29} \Sdot{60}{30} \Sdot{61}{31} \Sdot{62}{32}
  \Sdot{63}{33} \Sdot{64}{15} \Sdot{65}{16} \Sdot{66}{17}
  \Sdot{67}{18} \Sdot{68}{19} \Sdot{69}{20} \Sdot{70}{22}
  \Sdot{71}{23} \Sdot{72}{24} \Sdot{73}{28} \Sdot{74}{29}
  \Sdot{75}{30} \Sdot{76}{31} \Sdot{77}{32} \Sdot{78}{33}
  \Sdot{79}{15} \Sdot{80}{16} \Sdot{81}{17} \Sdot{82}{18}
  \Sdot{83}{19} \Sdot{84}{20} \Sdot{85}{21} \Sdot{86}{22}
  \Sdot{87}{23} \Sdot{88}{24} \Sdot{89}{25} \Sdot{90}{26}
  \Sdot{91}{27} \Sdot{92}{28} \Sdot{93}{29} \Sdot{94}{30}
  \Sdot{95}{31} \Sdot{96}{32} \Sdot{97}{33} \Sdot{98}{15}
  \Sdot{99}{16} \Sdot{100}{17} \Sdot{101}{29} \Sdot{102}{30}
  \Sdot{103}{31} \Sdot{104}{32} \Sdot{105}{33} \Sdot{106}{15}
  \Sdot{107}{16} \Sdot{108}{17} \Sdot{109}{29} \Sdot{110}{30}
  \Sdot{111}{31} \Sdot{112}{32} \Sdot{113}{33} \Sdot{114}{15}
  \Sdot{115}{16} \Sdot{116}{17} \Sdot{117}{29} \Sdot{118}{30}
  \Sdot{119}{31} \Sdot{120}{34} \Sdot{121}{35} \Sdot{122}{36}
  \Sdot{123}{37} \Sdot{124}{39} \Sdot{125}{40} \Sdot{126}{41}
  \Sdot{127}{42} \Sdot{128}{43} \Sdot{129}{44} \Sdot{130}{45}
  \Sdot{131}{46} \Sdot{132}{56} \Sdot{133}{57} \Sdot{134}{58}
  \Sdot{135}{59} \Sdot{136}{60} \Sdot{137}{61} \Sdot{138}{41}
  \Sdot{139}{42} \Sdot{140}{43} \Sdot{141}{57} \Sdot{142}{58}
  \Sdot{143}{59} \Sdot{144}{60} \Sdot{145}{61} \Sdot{146}{41}
  \Sdot{147}{42} \Sdot{148}{43} \Sdot{149}{57} \Sdot{150}{58}
  \Sdot{151}{59} \Sdot{152}{60} \Sdot{153}{61} \Sdot{154}{41}
  \Sdot{155}{42} \Sdot{156}{43} \Sdot{157}{44} \Sdot{158}{45}
  \Sdot{159}{46} \Sdot{160}{56} \Sdot{161}{57} \Sdot{162}{58}
  \Sdot{163}{59} \Sdot{164}{60} \Sdot{165}{61} \Sdot{166}{41}
  \Sdot{167}{42} \Sdot{168}{43} \Sdot{169}{44} \Sdot{170}{45}
  \Sdot{171}{46} \Sdot{172}{47} \Sdot{173}{48} \Sdot{174}{50}
  \Sdot{175}{51} \Sdot{176}{52} \Sdot{177}{53} \Sdot{178}{55}
  \Sdot{179}{56} \Sdot{180}{57} \Sdot{181}{58} \Sdot{182}{59}
  \Sdot{183}{60} \Sdot{184}{61} \Sdot{185}{41} \Sdot{186}{42}
  \Sdot{187}{43} \Sdot{188}{57} \Sdot{189}{58} \Sdot{190}{59}
  \Sdot{191}{60} \Sdot{192}{61} \Sdot{193}{41} \Sdot{194}{42}
  \Sdot{195}{43} \Sdot{196}{57} \Sdot{197}{58} \Sdot{198}{59}
  \Sdot{199}{60} \Sdot{200}{61} \Sdot{201}{41} \Sdot{202}{42}
  \Sdot{203}{43} \Sdot{204}{44} \Sdot{205}{45} \Sdot{206}{46}
  \Sdot{207}{56} \Sdot{208}{57} \Sdot{209}{58} \Sdot{210}{59}
  \Sdot{211}{60} \Sdot{212}{61} \Sdot{213}{41} \Sdot{214}{42}
  \Sdot{215}{43} \Sdot{216}{44} \Sdot{217}{45} \Sdot{218}{46}
  \Sdot{219}{47} \Sdot{220}{48} \Sdot{221}{49} \Sdot{222}{50}
  \Sdot{223}{51} \Sdot{224}{52} \Sdot{225}{53} \Sdot{226}{54}
  \Sdot{227}{55} \Sdot{228}{56} \Sdot{229}{57} \Sdot{230}{58}
  \Sdot{231}{59} \Sdot{232}{60} \Sdot{233}{61} \Sdot{234}{41}
  \Sdot{235}{42} \Sdot{236}{43} \Sdot{237}{44} \Sdot{238}{45}
  \Sdot{239}{46} \Sdot{240}{56} \Sdot{241}{57} \Sdot{242}{58}
  \Sdot{243}{59} \Sdot{244}{62} \Sdot{245}{62}
\end{tikzpicture}
  \end{center}
  \caption{Trace of the exection of \texttt{ms.asm} on input \texttt{ms10.in}}
  \label{fig:ms10}
\end{figure}

\newpage

\appendix

\section{ms.asm : assembler code}
\label{asmcode}
\lstinputlisting[language=asm,numbers=left,frame=single]{ms-minimal.asm}


\bibliography{ms}


\end{document}
