ms.spk             Jan 12, 2020


Instance: integers m,n,Tin and boolean p[0],...,p[m-1]

Problem: Schedule m jobs on n machines to minimize the latest finish time (makespan) T.
         The time of job i is p[i]+1

Output:  The value T and a job schedule x[m,n]: x[i,j]=1 if job i is scheduled 
         on machine j and zero otherwise
         return w=0

Since the job times are in {1,2} a list decreasing schedule is optimum.
This is achieved by the code ms.spk derived from the C code ms.c

A time_bound of 29m+13 is sufficient.

------------------------------------------------------------------------------------

For the following examples you will either need to use full paths to
the tools or add the sparktope bin directory to your path with e.g.

% PATH=$(realpath ../../bin):$PATH

Example 1: m=10, n=3, T=5

directory: ms10

input file: ms10.in
parameter file: inputs 10.param     - handles all inputs for m=10 

% buildpoly ../ms.spk 10.param >! ms.lp

The lp file has about 380K constraints and size 21MB.

% input2obj -p 10.param ms10.in >! ms10.json
% runpoly -f ms.lp ms10.json >! ms10.sol      (run glpsol)
or
% runpoly -f -c ms.lp ms10.json >! ms10.sol      (run cplex)

% lpstatus ms10.sol
zzobj=8
S[62,319]=1
S[62,320]=1
halted=1
w=0
Which indicates that the program halted normally at a return statement on line 62
The objective value zzobj is 2*(# of jobs with processing  time 2)

To get the makespan T

% grep T ms10.sol | tail -4
T[3,320]=0
T[2,320]=1
T[1,320]=0
T[0,320]=1

which is T in binary, ie T=5.

% grep "S\[62" ms10.sol | head -3
S[62,321]=1
S[62,244]=1
S[62,245]=1

indicates the return statement at line 62 was reached after 244 steps.

% plotlog ms10.sol >! ms10.pdf

gives a full trace of the run.

send comments to: david avis avis@cs.mcgill.ca
