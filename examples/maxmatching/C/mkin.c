/* step count bounds for mm.spk */

#include <math.h>
#include <stdio.h>
#define TRUE 1
#define FALSE 0

void main()
{
int n,m,M,i;
int a,b;
int A[100][100];

printf("\n enter n vertices m edges M matching edges:");
scanf ("%d %d %d",&n,&m,&M);
printf("\n");
for (a=0;a<=n;a++)
 for (b=0;b<=n;b++)
   A[a][b]=0;

for (i=1;i<=m;i++)
  {
   scanf ("%d %d",&a,&b);
   printf("\n%d %d",a,b);
   if(a<b)
    A[a][b]=1;
   else
    A[b][a]=1;
  }
for (i=1;i<=M;i++)
  {
   scanf ("%d %d",&a,&b);
   printf("\n%d %d",a,b);
   if(a>b)
    A[a][b]=1;
   else
    A[b][a]=1;

  }
printf("\n");
for (a=0;a<n;a++)
  {
   printf("\n");
   for (b=0;b<n;b++)
     printf("%d ",A[a][b]);
  }
printf("\n");
printf("\nmatrix a[%d,%d] <- {",n,n);
for (a=0;a<n;a++)
  {
   printf("\n    {");
   for (b=0;b<n;b++)
    {
      printf("%d",A[a][b]);
      if(b!=n-1)
         printf(",");
    }
    printf("}");
    if(a<n-1)
         printf(",");
      else
         printf("}");
  }

printf("\n");
return;
}
