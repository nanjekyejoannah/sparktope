
| Compiler | Example    | Ex. ver | w | n |    t | Bounds                   |    rows | columns | non-zeros |
|      1.0 | am         | 248ea7d | 3 | 6 | 2000 | main.LB=220, init.UB=290 | 6672035 | 1289907 |  24681360 |
|      1.0 | matrix-for | 1352d80 | 3 | 6 |  425 |                          |  558851 |   58204 |   2170136 |
|      1.0 | reg        | f38fa56 | 3 | 8 | 1088 | main.LB=256, init.UB=320 | 1169319 |  266593 |   4501788 |


