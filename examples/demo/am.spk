#Dec 25 2016     v1.0

# one iteration of Edmonds' algorithm
# input a graph and matching, output 0 if maximum and 1 otherwise
# while/while shrinking              
# inline blossom matching, matching of tip set correctly
# array indices are from 0,...,n-1
# contains phases for init  and main

phase init do

input matrix a[n,n]
triangular matrix A[n,n]   # adjacency matrix
array  odd[n]
array  marked[n]    # one for marked vertices
array  F[n]         # one for vertices in forest F
array shrunk[n]     # shrunk[v]=1 for shrunk, ie. dead, vertex

int array match[n]  # match[v]=v for unmatched vertex 
int array parent[n] # parent[v]=v for root
output bool w

bool x
bool y
bool z
bool progress
bool swap
bool edge
bool doneW
bool doneV
bool tip

int i
int j
int k
int V
int W
int X
int Y
int row
int col

A[*,*]<-0       
shrunk[*]<-0            
for i<-0,$n-1$ do      
       match[[i]]<-i     # denotes unmatched edge
done

for i<-0,$n-2$ do      # allow for an input matching
       for j<-inc(i),$n-1$ do
          A[i,j]<-a[i,j]   # can be deleted if using greedy matching
          if a[j,i] then   # matching edge: j  i
             match[[i]] <- j
             match[[j]] <- i
          endif
       done
done
 
progress<-1

done

# phase init finsished, main loop starts here #
phase main do

while progress do      # find aug path and exit of find blossom and  shrink
    odd[*]<-0 
    marked[*]<-0
    for i<-0,$n-1$ do      # reinitialize              
     if shrunk[i] then
         marked[i]<-1
     else              # only reinitialize live vertices
           parent[[i]]<-i
           F[i]<- i = match[[i]]   #unmatched alive vertices initialize F
     endif
    done


  progress<-0
  V<-0
  doneV<-0
  while !progress and !doneV do         # unexplored vertex
        x<-!marked[V] and !odd[V]
        if x and  F[V] then  # unexplored edge
            marked[V]<-1 # mark V
            W<-0
            doneW<-0
            while !progress and !doneW do
               if V!= W and !shrunk[W] then   # W is still alive
                   if V < W then
                      edge<-A[V,W]
                   else
                      edge<-A[W,V]
                  endif
                  if edge and !odd[W] then #unmarked edge VW
                     if  !F[W] then   # W not in F
                         X<-match[[W]]        # exposed nodes all in F
                         parent[[W]]<-V       # add W and X to F
                         parent[[X]]<-W       # add W and X to F
                         F[W]<-1
                         F[X]<-1
                         odd[W]<-1
                         odd[X]<-0
                     else                         # W in F
                         i<-V     # find roots for V and W
                         while i != parent[[i]] do
                                 i<- parent[[i]]
                         done
                          k<-W
                         while k != parent[[k]] do
                              k<- parent[[k]]
                         done
                         if  i != k then        # alternating path
                                    return w @ 1          # success !
                             else                          # shrink blossom
                                  X<-parent[[V]]    #reverse tree edges
                                  parent[[V]]<-W 
                                  while V != X  do #reverse edges from W to root
                                      Y<-parent[[X]]
                                      parent[[X]]<-V      #reverse edge
                                      V<-X
                                      X<-Y
                                   done          # end reverse tree edges
                                   V<-match[[W]]
                                   tip<-0
                                   while V != W do  #traverse and shrink cycle
                                      shrunk[V]<-1
                                      if !tip then
                                           if !odd[V] and parent[[V]] != match[[V]] then
                                                 tip<-1
                                                 if match[[V]]=V then  #V is the tip
                                                       match[[W]]<-W
                                                 else
                                                       match[[W]]<-match[[V]]
                                                 endif
                                           endif
                                       endif
                                       j<-0
                                       swap<-0       # see comments below for pedestrian shrinking
                                       col<-W
                                       while j<V do         # shrink V to W
                                              if W=j then
                                                   swap<-1
                                                   row<-W
                                              else   
                                                   if swap then
                                                       col<-j
                                                   else
                                                       row<-j
                                                   endif
                                                   A[row,col]<-A[row,col] or A[j,V]  # copy to shrunk vertex
                                              endif
                                           j++
                                       done
                                                     # note: j=V, no need to reset swap 
                                       while j != $n-1$ do
                                           j++                 # increment here to skip j=V
                                              if W=j then
                                                   swap<-1
                                                   row<-W
                                              else   
                                                   if swap then
                                                       col<-j
                                                   else
                                                       row<-j
                                                   endif
                                                   A[row,col]<-A[row,col] or A[V,j]  # copy to shrunk vertex
                                              endif
                                       done
                                   V<-parent[[V]]
                                   done                # traverse and shrink cycle
                             endif          # if i != k alternating path (or blossom)
                             progress<-1       # iteration over, we got path or blossom
                        endif               # W in F
                     endif                  # unmarked VW
                  endif                     # if V!=W
                   if W = $n-1$ then
                      doneW<-1
                   else
                      W++
                   endif
                 done                       # while W
             endif                          # unexplored edge
       if V = $n-1$ then
         doneV<-1
       else
         V++
    endif
  done                                      # unexplored vertex
done                                       # only one iteration required

return w @ 0    # no perfect matching
 
done            # init phase main
#pedestrian shrinking
#
#  for j<-0,$n-1$ do     #shrink V onto W in A
#        if W!=j and V!=j then
#           if W < j then
#              r<-W
#              s<-j
#           else
#              r<-j
#              s<-W
#           endif
#           if V < j then
#              p<-V
#              q<-j
#           else
#              p<-j
#              q<-V
#           endif
#           A[r,s]<-A[r,s] or A[p,q]  # copy to shrunk vertex
#         endif    # W!=j
#  done        # for j
