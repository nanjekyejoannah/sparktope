class Op:
    def __init__(self, **args):
        self.name = args['name']
        self.inequalities = args['inequalities']
        self.types = args['types']
        self.columns = args.get('columns')
        if 'special' in args:
            self.special=True
        else:
            self.special=False
        if 'tag' in args:
            self.tag=args['tag']
        else:
            self.tag=args['name']
        if 'temp_bits' in args:
            self.temp_bits=args['temp_bits']
        else:
            self.temp_bits=0

    def __str__(self):
        return "{0:s}".format(self.name)

    def return_type(self):
        return self.types[-1]

    # number arguments from 1, for sorta semi-consistency
    def arg_type(self,i):
        return self.types[i-1]
