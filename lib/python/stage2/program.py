from symboltable import SymbolTableTable
from stage2.statement import StatementList
from linprog.intutils import *
from linprog.constraintutils import *
import itertools
import re

class Program:
    def __init__(self, **keywords):
        self.statements = {}
        self.lines = []
        self.next = {}
        self.last_line = None
        self.stmt_labels = {}
        self.returns = []
        self.ops=keywords['ops']
        self.params=keywords['params']

        self.symbols = SymbolTableTable(self.params['word_size'])
        for name in ['input', 'output', 'var', 'internal', 'constant']:
            self.symbols.add(name)

    def __len__(self):
        return len(self.statements)

    def __getitem__(self, idx):
        return self.statements[idx]

    def reachable(self, idx, time):
        stmt=self.statements[idx]
        phases=stmt.phases[:]
        while len(phases)>0:
            active=phases.pop()
            if active in self.params:
                bounds=self.params[active]
                if 'LB' in bounds and bounds['LB'] > time:
                    return False
                if 'UB' in bounds and bounds['UB'] < time:
                    return False
        return True
    
    def declare(self, prefix, varname, type, typeargs=[]):
        outargs = []
        parts = varname.split(":")
        if len(parts)==1:
            outtype=type
            outvar=varname
        else:
            outtype='int'
            outvar=parts[0]

        self.symbols.declare(prefix, outvar, outtype, typeargs)

    def mark_modified(self, varname, line):
        var=self.symbols['var'][varname]
        var.mark_used('modified',line)

    def declare_index(self, index, itype, line):
        if integerish(index):
            prefix='constant'
        elif index in self.symbols['input']:
            prefix='input'
        else:
            prefix='var'

        self.symbols.declare(prefix, index, 'int')

        var=self.symbols[prefix][index]
        var.mark_used('{0:s}_index'.format(itype),line)

    def sizeof(self, varname):
        return self.symbols.sizeof(varname)

    def typeof(self, varname):
        return self.symbols.typeof(varname)

    def prefix(self, var):
        return self.symbols.prefix(var)

    def declare_IO(self, var, type):
        self.declare('input',var, type)

    def split_at(self, var, time, type):

        return self.symbols.split_at(var, time, type)

    def label_index(self,name):
        return self.stmt_labels[name]

    def append(self, stmt):

        line = stmt.line
        
        if stmt.stype == 'return':
            self.returns.append(line)

        self.lines.append(line)

        self.statements[line]=stmt

        if self.last_line:
            self.next[self.last_line]=line
            
        self.last_line = line

    def vars(self):
        return self.symbols['var'].itervalues()

    def indices(self, itype):
        return itertools.ifilter(
            lambda var:  var.used('{0:s}_index'.format(itype)) != [],
            itertools.chain(
            self.symbols['constant'].itervalues(),
            self.symbols['input'].itervalues(),
            self.symbols['var'].itervalues()))

    def output_params(self):

        print "set INPUTS := {" + ",".join(["'{0:s}'".format(var) for var in
                                            self.expanded_inputs()]) + "};"

        print "set OUTPUTS := {" + ",".join(["'{0:s}'".format(var) for var in
                                             self.expanded_outputs()]) + "};"

        print "set VARS := {" + \
                ",".join(["'{0:s}'".format(var.name)
                          for var in self.vars()]) + "};"

        print "set LINES := {" + \
                ",".join(["{0:d}".format(j)
                          for j in self.lines]) + "};"
        
        print "set ROW_INDICES := {" + \
                ",".join(["{0:s}".format(sanitize_index(var.name))
                          for var in self.indices('row')]) + "};"

        print "set COL_INDICES := {" + \
                ",".join(["{0:s}".format(sanitize_index(var.name))
                          for var in self.indices('col')]) + "};"

        print "param tmax := {0:d};".format(self.params['time_bound'])
        print "param bits := {0:d};".format(self.params['word_size'])
        print "param c{INPUTS};"
        print "param d{OUTPUTS};"

    def expanded_sym(self,name,time):
        index = self.symbols[name]
        bits = []
        for var in index.itervalues():
            bits += self.split_at(var.name,time,var.type)
        return bits

    def expanded_inputs(self):
        return self.expanded_sym('input',-1)

    def expanded_outputs(self):
        return self.expanded_sym('output',-1)

    def output_objective(self,out_format):

        if out_format=='lp':
            print "maximize\n\t obj: zzobj\nSuch that"
            
        else:
            print "maximize\n\t obj: zzobj;\n"            
            fmtstr = "+{c:s}*{v:s}"
            objstr = "OBJDEF: zzobj= 0 "
            for bit in self.expanded_inputs():

                objstr += fmtstr. \
                          format(c=bit.parameterize('c',out_format),
                                 v=bit.stringify(out_format))

            for bit in self.expanded_outputs():
                objstr += fmtstr. \
                          format(c=bit.parameterize('d',out_format),
                                 v=bit.stringify(out_format))

            print objstr+";"


    def output_vars(self):
        end_time=self.params['time_bound']
        W=self.params['word_size']

        print "var S{LINES,1..tmax},>=0,<=1;"
        print "var halted,>=0,<=1;"
        print "var zzobj,>=0;"                
        print "var _K{VARS,1..tmax},>=0,<=1;"
        print "var _Mc{ROW_INDICES,1..tmax},>=0,<=1;"
        print "var _Nc{COL_INDICES,1..tmax},>=0,<=1;"
        print "var _ot{{0..{0:d},1..tmax}},>=0,<=1;".format(4*W-3-1)
        for (key,var) in itertools.chain(
                self.symbols['input'].iteritems(),
                self.symbols['output'].iteritems()):
            if var.type == 'int':
                print("var {name:s}{{0..bits-1}},>=0,<=1;".format(
                    name=var.name))
            elif var.type == 'bool':
                print("var {name:s},>=0,<=1;".format(name=var.name))
            elif var.type == 'array':
                print("var {name:s}{{0..{size:d}}},>=0,<=1;".format(
                    name=var.name,size=self.symbols.sizeof(var.name)-1))
            else:
                # matrix
                pair = self.symbols.sizeof(var.name)
                print("var {name:s}{{0..{rows:d},0..{cols:d}}},>=0,<=1;".format(name=var.name, rows=pair[0]-1, cols=pair[1]-1))



        # variables get a time dimension
        for (key,var) in itertools.chain(
                self.symbols['var'].iteritems(),
                self.symbols['internal'].iteritems()):
            if var.type == 'int':
                print("var {name:s}{{0..bits-1,0..tmax-1}},>=0,<=1;".format(
                    name=var.name))
            elif var.type == 'bool':
                print("var {0:s}{{0..tmax-1}},>=0,<=1;".format(var.name))
            elif var.type == 'array':
                # array
                print("var {name:s}{{0..{size:d},0..tmax-1}},>=0,<=1;".format(
                    name=var.name,size=self.sizeof(var.name)-1))
            else:
                # matrix
                pair = self.symbols.sizeof(var.name)
                print("var {name:s}{{0..{rows:d},0..{cols:d},0..tmax-1}},>=0,<=1;".format(name=var.name, rows=pair[0]-1, cols=pair[1]-1))

    def output_bounds(self, fmt):

        tmax = self.params['time_bound']

        print("\nBounds");

        # note most of this first part is hardcoded for LP format

        for i in self.lines:
            for t in range(1,tmax):
                print "0 <= S#{i:d}#{t:d} <= 1".format(i=i,t=t)

        print "0 <= halted <=1"
        
        for v in  self.vars():
            for t in range(1,tmax):
                print "0 <= _K#{nm:s}#{t:d} <= 1". \
                    format(nm=v.name,t=t)

        for v in  self.indices('row'):
            for t in range(1,tmax):
                print "0 <= _Mc#{nm:s}#{t:d} <= 1". \
                    format(nm=v.name,t=t)

        for v in  self.indices('col'):
            for t in range(1,tmax):
                print "0 <= _Nc#{nm:s}#{t:d} <= 1". \
                    format(nm=v.name,t=t)

        for j in range(0,4*self.params['word_size']-3):
            for t in range(1,tmax):
                print "0 <= _ot#{j:d}#{t:d} <= 1". \
                    format(j=j,t=t)

        for type in ['input', 'output']:
            for bit in self.expanded_sym(type,-1):
                print "0 <= {0:s} <=1".format(bit.stringify(fmt))

        for type in ['var', 'internal']:
            for t in range(0,self.params['time_bound']):
                for bit in self.expanded_sym(type,t):
                    print "0 <= {0:s} <=1".format(bit.stringify(fmt))

        print "End"
    def output_epilog(self):

        print("solve;")
        print('printf{_t in 1..tmax, _i in LINES : S[_i,_t] != 0} "S[%02d,%02d]=%g\\n",_i,_t,S[_i,_t];')
        print('printf "halted=%g\\n",halted;')

        print('printf{_v in ROW_INDICES, _t in 1..tmax : _Mc[_v,_t] !=0} "_I[%s,%02d]=%g\\n",_v,_t,_Mc[_v,_t];')

        print('printf{_v in COL_INDICES, _t in 1..tmax : _Nc[_v,_t] !=0} "_I[%s,%02d]=%g\\n",_v,_t,_Nc[_v,_t];')

        if len(self.symbols['var']) > 0:
            print('printf{_v in VARS, _t in 1..tmax : _K[_v,_t] !=0} "_K[%s,%02d]=%g\\n",_v,_t,_K[_v,_t];')

            print("display " + ",".join([key for key in self.symbols['var']])+";")
        if len(self.symbols['internal']) > 0:
            print("display " + ",".join([key for key in self.symbols['internal']])+";")
        print("display obj;")
        if len(self.symbols['input']) > 0:
            print("display " + ",".join([key for key in self.symbols['input']])+";")
        if len(self.symbols['output']) > 0:
            print("display " + ",".join([key for key in self.symbols['output']])+";")
        print("end;")

    # output constraints to get the value of a non-array variable at t
    # either updating or preserving

    def update_memory (self, i, t, var, ineq ):
        instr = self[i].stype
        op = self[i].op
        destbits = self.split_at(var.name, t, var.type)

        if instr == 'set' and var.name == self[i].args[0]:

            # array and matrix refs are taken care of elsewhere
            if op.special and op.name == 'set_array_ref':
                self.update_from_array(i, t, ineq)
            elif op.special and op.name == 'set_matrix_ref':
                self.update_from_matrix(i, t, ineq)
            elif op.special and op.name == 'set_row_ref':
                self.update_from_row(i, t, ineq)
            else:

                inputs = []
                # first argument is S[i,t]
                j = 2
                for v in self[i].args[1:]:
                    inputs += self.split_at(v , t-1, op.arg_type(j))
                    j = j+1

                label="{op:s}_{0:d}_{1:d}_{{0:d}}".format(i,t,op=op.tag)

                temps=self.temp_vars(i,t)

                if op.name=='set_decw':
                    # we just reverse the arguments to do a decrement
                    ineq.load_op(self.ops['set_incw'], label,
                                 [S(i,t)] + destbits + inputs+temps)
                else:
                    ineq.load_op(op, label, [S(i,t)] + inputs + destbits+temps)

        elif instr == 'array_set' and var.name == self[i].args[0]:

            (array, array_index, val) = self[i].args
            array_size = self.sizeof(array)

            for j in range(0,array_size):
                ineq.array_temp_constraints('M',
                                            i, 0, j, t,
                                            array,
                                            array_index)

                ineq.append('AS_c_{i:d}_{t:d}_{j:d}'.format(j=j,t=t,i=i),
                            1,'>=',
                            S(i,t),
                            self.symbols.bool_at(val, t-1),
                            (-1, self.symbols.array_at(array,j,t)),
                            (-1,M(0,j,t)))

                ineq.append('AS_d_{i:d}_{t:d}_{j:d}'.format(j=j,t=t,i=i),
                            1,'>=',
                            S(i,t),
                            (-1, self.symbols.bool_at(val, t-1)),
                            self.symbols.array_at(array,j,t),
                            (-1,M(0,j,t)))

                ineq.append('AS_e_{i:d}_{t:d}_{j:d}'.format(j=j,t=t,i=i),
                            2,'>=',
                            S(i,t),
                            (1, self.symbols.array_at(array,j,t-1)),
                            (-1, self.symbols.array_at(array,j, t)),
                            M(0,j,t))

                ineq.append('AS_f_{i:d}_{t:d}_{j:d}'.format(j=j,t=t,i=i),
                             2,'>=',
                             S(i,t),
                             (-1, self.symbols.array_at(array,j,t-1)),
                             (1, self.symbols.array_at(array,j, t)),
                             M(0,j,t))

        elif instr == 'matrix_set' and var.name == self[i].args[0]:

            (matrix, row_index, col_index, val)  = self[i].args

            self.matrix_N_M_constraints(matrix,i,0,t,
                                        row_index, col_index,
                                        ineq)

            ineq.matrix_set_constraints(self.symbols,
                                        matrix, i, 0, val, t)

        elif instr == 'row_set' and var.name == self[i].args[0]:
            self.update_to_row(i,t,ineq)
        elif instr in ['array_init', 'matrix_init'] and var.name == self[i].args[0]:
            (var,  val) = self[i].args
            ineq.am_init_constraints(self.symbols, i, var, val, t)
        else:
            # not a memory modifying instruction
            pass

    def temp_vars (self, line, time):
        op = self[line].op
        temp_bits=0
        if op.name=='set_decw':
            temp_bits=self.ops['set_incw'].temp_bits
        else:
            temp_bits=op.temp_bits

        return [ otemp(j,time) for j in range(0,temp_bits)]

    def update_to_row(self, line, time, ineq):
        (matrix, row_index, val)  = self[line].args

        (rows,cols) = self.sizeof(matrix)

        for r in range(0,rows):
            ineq.array_temp_constraints('M',
                                        line, 0, r, time,
                                        matrix, row_index)

        ineq.row_set_constraints(self.symbols,
                                 matrix, line, val, time)


    def update_from_row(self, line, time, ineq ):

        (target, matrix, row_index) = self[line].args

        (rows,cols) = self.sizeof(matrix)

        for r in range(0,rows):
            ineq.array_temp_constraints('M',
                                        line, 0, r, time,
                                        matrix,
                                        row_index)


        ineq.row_ref_constraints(self.symbols,
                                 target,
                                 matrix, line, time)

    def update_from_array(self, i, t, ineq ):

        array = self[i].args[1]
        array_size = self.sizeof(array)
        target = self[i].args[0]
        array_index = self[i].args[2]

        for j in range(0,array_size):
            ineq.array_temp_constraints('M',i, 0, j, t,
                                        array, array_index)

            ineq.append('AR_g_{i:d}_{t:d}_{j:d}'.format(j=j,t=t,i=i),1,'>=',
                        S(i,t),
                        self.symbols.bool_at(target, t),
                        (-1, self.symbols.array_at(array,j,t)),
                        (-1,M(0,j,t)))

            ineq.append('AR_h_{i:d}_{t:d}_{j:d}'.format(j=j,t=t,i=i),1,'>=',
                        S(i,t),
                        (-1,self.symbols.bool_at(target, t)),
                        self.symbols.array_at(array,j,t),
                        (-1,M(0,j,t)))

    def matrix_N_M_constraints(self, matrix, i, count, t,
                               row_index, col_index,
                               ineq):

        (rows, cols) = self.sizeof(matrix)

        for j in range(0,rows):
            ineq.array_temp_constraints('M',i, count, j, t,
                                        matrix,
                                        row_index)

        for j in range(0,cols):
            ineq.array_temp_constraints('N',i, count, j, t,
                                        matrix,
                                        col_index)

    def update_from_matrix(self, i, t, ineq ):

        (target, matrix, row_index, col_index)  = self[i].args

        self.matrix_N_M_constraints(matrix,i,0,t,
                                    row_index,
                                    col_index, ineq)

        ineq.matrix_ref_constraints(self.symbols,
                                    self.symbols.bool_at(target, t),
                                    matrix, i, 0, t)

    def keep_constraints(self, var, t, ineq):

        destbits = self.split_at(var.name, t, var.type)

        ineq.K_constraints(var, t);

        inputs = self.split_at(var.name, t-1, var.type)
        if var.type == 'int':
            op = self.ops['set_copyw']
        else:
            op = self.ops['set_copy']

        if var.type in ['array', 'matrix']:
            for j in range(0, len(destbits)):
                ineq.load_op(op,
                             "ka_{array:s}_{time:d}_{index:d}_{{0:d}}". \
                             format(array=var.name,
                                    index=j,
                                    time=t),
                             [K(var,t),
                              inputs[j],  destbits[j]])

        else:
            ineq.load_op(op, "k_{0:s}_{1:d}_{{0:d}}". \
                         format(var.name,t),
                         [K(var,t)] + inputs + destbits)

    def intparam(self, term):
        return self.params.eval_expr(term)

    macro_re = re.compile("[$](.*)[$]")
    def param_expand(self, str):
        match = Program.macro_re.match(str)
        if match == None:
            if str in self.params:
                return self.params[str]
            else:
                return str
        else:
            inner = match.group(1)
            return self.params.eval_expr(inner.replace("\\$","$"))
