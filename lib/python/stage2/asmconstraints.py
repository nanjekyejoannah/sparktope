# sets of linear constraints, specialized for running assembly

from linprog.constraintutils import *
from linprog.constraintset import ConstraintSet

class AsmConstraints(ConstraintSet):
    def __init__(self, word_size, symbols, file=None, output='gmpl'):
        ConstraintSet.__init__(self, file, output)
        self.symbols = symbols
        self.word_size = word_size

    def array_temp_constraints(self, var, line, count, j, t, array, index):

        index_bits = self.symbols.split_at(index,t-1,'int')
        bits = index_bits[::-1]

        for k in range(0,self.word_size):
            terms = [index_control(var,index,t)]+T(1,j,k,bits[k]) \
                            + [(-1,atemp(var,count,j,t))]
            self.append_once('{var:s}_{index:s}_{count:d}_{t:d}_{j:d}_a{k:d}'.
                        format(j=j,k=k,t=t,var=var,index=str(index),count=count),
                                     1, '>=', *terms)
        terms = [index_control(var,index,t)]
        for  k in range(0,self.word_size):
            terms += T(-1,j,k,bits[k])

        terms += [atemp(var,count,j,t)]
        self.append_once('{var:s}_{index:s}_{count:d}_{t:d}_{j:d}_b'. \
                    format(j=j,t=t, var=var, index=str(index), count=count),1,
                    '>=', *terms)

    def am_init_constraints(self, symbols, line, var, val, time):

        type = symbols.typeof(var)
        destbits = symbols.split_at(var, time, type)

        k=len(destbits)

        label='AI_{line:d}_{var:s}_{time:d}'.format(var=var, line=line, time=time)

        if int(val)==0:
            self.append(label, k, '>=', (k,S(line,time)), *destbits)
        elif int(val)==1:
            self.append(label, 0, '<=', (-1*k,S(line,time)), *destbits)
        else:
            raise TypeError(val)




    def matrix_ref_constraints(self, symbols, raw_target,
                               matrix, i, count, t):

        (rows, cols) = symbols.sizeof(matrix)
        triangular = symbols.isTriangular(matrix)
        for j1 in range(0,rows):
            for j2 in range(j1+1 if triangular else 0,cols):
                label='MR_{i:d}_{c:d}_{t:d}_{j1:d}_{j2:d}_{{0:s}}'. \
                        format(j1=j1,j2=j2,t=t,i=i,c=count)

                self.append(
                    label.format('a'),
                    1,'>=',S(i,t),raw_target,
                    (-1, symbols.matrix_at(matrix,j1,j2,t-1)),
                    (-1,M(count,j1,t)),
                    (-1,N(count,j2,t)))

                self.append(
                    label.format('b'),
                    1,'>=', S(i,t),
                    (-1,raw_target),
                    (+1, symbols.matrix_at(matrix,j1,j2,t-1)),
                    (-1,M(count,j1,t)),
                    (-1,N(count,j2,t)))


    def matrix_set_constraints(self, symbols,
                               matrix, i, count, value, t):

        (rows, cols) = symbols.sizeof(matrix)

        triangular = symbols.isTriangular(matrix)
        for j1 in range(0,rows):
            for j2 in range(j1+1 if triangular else 0,cols):
                label='MS_{i:d}_{c:d}_{t:d}_{j1:d}_{j2:d}_{{0:s}}'. \
                        format(j1=j1,j2=j2,t=t,i=i,c=count)
                self.append(
                    label.format('a'),1,'>=',
                    S(i,t),
                    symbols.bool_at(value, t-1),
                    (-1, symbols.matrix_at(matrix,j1,j2,t)),
                    (-1,M(count,j1,t)),
                    (-1,N(count,j2,t)))

                self.append(
                    label.format('b'),1,'>=',
                    S(i,t),
                    (-1,symbols.bool_at(value, t-1)),
                    (+1, symbols.matrix_at(matrix,j1,j2,t)),
                    (-1,M(count,j1,t)),
                    (-1,N(count,j2,t)))

                self.append(
                    label.format('c'),2,'>=',
                    S(i,t),
                    (+1, symbols.matrix_at(matrix,j1,j2,t-1)),
                    (-1, symbols.matrix_at(matrix,j1,j2,t)),
                    (+1,M(count,j1,t)))

                self.append(
                    label.format('d'),2,'>=',
                    S(i,t),
                    (-1, symbols.matrix_at(matrix,j1,j2,t-1)),
                    (+1, symbols.matrix_at(matrix,j1,j2,t)),
                    (+1,M(count,j1,t)))

                self.append(
                    label.format('e'),2,'>=',
                    S(i,t),
                    (+1, symbols.matrix_at(matrix,j1,j2,t-1)),
                    (-1, symbols.matrix_at(matrix,j1,j2,t)),
                    (+1,N(count,j2,t)))

                self.append(
                    label.format('f'),2,'>=',
                    S(i,t),
                    (-1, symbols.matrix_at(matrix,j1,j2,t-1)),
                    (+1, symbols.matrix_at(matrix,j1,j2,t)),
                    (+1,N(count,j2,t)))

    # set N[j,t]=0 in matrix set constraints
    def row_set_constraints(self, symbols, matrix, i, val, t):

        (rows, cols) = symbols.sizeof(matrix)
        source_bits = symbols.split_at(val,t-1, 'int')
        source_bits.reverse()

        for r in range(0,rows):
            for c in range(0,cols):
                label='RS_{{0:s}}_{i:d}_{t:d}_{r:d}_{c:d}'. \
                        format(r=r,c=c,t=t,i=i)
                self.append(
                    label.format('a'),1,'>=',
                    S(i,t),
                    source_bits[c],
                    (-1, symbols.matrix_at(matrix,r,c,t)),
                    (-1,M(0,r,t)))

                self.append(
                    label.format('b'),1,'>=',
                    S(i,t),
                    (-1,source_bits[c]),
                    (+1, symbols.matrix_at(matrix,r,c,t)),
                    (-1,M(0,r,t)))

                self.append(
                    label.format('c'),2,'>=',
                    S(i,t),
                    (+1, symbols.matrix_at(matrix,r,c,t-1)),
                    (-1, symbols.matrix_at(matrix,r,c,t)),
                    (+1,M(0,r,t)))

                self.append(
                    label.format('d'),2,'>=',
                    S(i,t),
                    (-1, symbols.matrix_at(matrix,r,c,t-1)),
                    (+1, symbols.matrix_at(matrix,r,c,t)),
                    (+1,M(0,r,t)))

    # set N[j,t]=0 in matrix ref constraints
    def row_ref_constraints(self, symbols, target, matrix, i, t):

        (rows, cols) = symbols.sizeof(matrix)
        dest_bits = symbols.split_at(target, t, 'int')

        dest_bits.reverse()

        for r in range(0,rows):
            for c in range(0,cols):
                label='RR_{{0:s}}_{i:d}_{t:d}_{r:d}_{c:d}'. \
                    format(r=r,c=c,t=t,i=i)

                self.append(
                    label.format('a'),
                    1,'>=',S(i,t),dest_bits[c],
                    (-1, symbols.matrix_at(matrix,r,c,t-1)),
                    (-1,M(0,r,t)))

                self.append(
                    label.format('b'),
                    1,'>=', S(i,t),
                    (-1,dest_bits[c]),
                    (+1, symbols.matrix_at(matrix,r,c,t-1)),
                    (-1,M(0,r,t)))

    def K_constraints(self, var, t):
        self.used_constraints(var, t, 'K', 'modified', -1)

    def used_constraints(self, var, t, outvar, usage_type, sign):

        OV=LPVariable('_{0:s}'.format(outvar),
                      'array',[var.name,t])

        lhs = (1-sign)/2
        terms = [(-1*sign,OV)]
        for line in var.used(usage_type):
            terms += [S(line,t)]

            self.append("{outvar:s}_{name:s}_{t:d}_a_{line:d}". \
                        format(outvar=outvar,
                               name=var.name, t=t,line=line),
                        lhs, ">=",
                        S(line,t), (-1*sign,OV))

        self.append("{outvar:s}_{name:s}_{t:d}_b". \
                    format(name=var.name, t=t,
                           outvar=outvar),
                    lhs, "<=", *terms)
