from linprog.intutils import *
from linprog.constraintutils import negsense

class Constraint:
    def __init__(self, tag, lhs, type, terms ):
        self.tag = tag
        self.lhs = int(lhs)
        self.type = type
        self.terms= {}
        self.line_length_limit=255

        for term in terms:
            # hack to tell strings/ints from tuples.
            if isinstance(term,(list,tuple)):
                coeff = term[0]
                var = term[1]
            else:
                coeff = 1
                var = term

            if integerish(var):
                self.lhs -= int(var)*coeff
            else:
                if var in self.terms:
                    # duplicate variable!
                    self.terms[var] += coeff
                else:
                    self.terms[var] = coeff

    def format_term(self, var, out_format, first):

        if first:
            plus = ''
        else:
            plus = '+'

        coeff = self.terms[var]
        if integerish(coeff):
            if coeff == 0:
                fmt = ''
            elif coeff == 1:
                fmt = plus + '{v:s}'
            elif coeff == -1:
                fmt = '-{v:s}'
            elif (coeff < 0):
                fmt = ' - {c:s}{o:s}{v:s}'
            else:
                fmt = plus + '{c:s}{o:s}{v:s}'
            outcoeff=str(abs(int(coeff)))
        else:
            if out_format == 'gmpl':
                thisplus = plus
            else:
                thisplus = ' '
            fmt =  thisplus + '{c:s}{o:s}{v:s}'
            outcoeff= coeff;


        return fmt.format(c=outcoeff,
                          o= '*' if out_format=='gmpl' else ' ',
                          v=var.stringify(out_format))

    def stringify(self, out_format='gmpl'):
        row = '{0:s}: '.format(self.tag)

        lines = []

        if len(self.terms) == 0:
            row += '0'
        else:
            keys = sorted(self.terms.keys())

            row += self.format_term(keys[0], out_format, True)

            for var in keys[1:]:
                if (len(row) > self.line_length_limit):
                    lines+=[row]
                    row=''

                row += self.format_term(var, out_format, False)


        return "\n".join(lines)+row+'{op:s} {rhs:d}{sep:s}'. \
            format(rhs=self.lhs,
                   op=negsense(self.type),
                   sep=';' if out_format=='gmpl' else '')

    def __str__(self):
        return self.stringify()

class ConstraintSet:
    def __init__(self, file=None, output='gmpl'):
        self.rows = {}
        self.file = file
        self.output = output
        self.seen = {}

    # support for loops over constraint sets
    # FIXME: throw error if streaming output
    def __iter__(self):
        import operator
        return (value for (key,value) in
                sorted(self.rows.items(),
                       key=operator.itemgetter(0)))

    def write_or_save(self, key, cnst):
        if self.file:
            self.file.write(cnst.stringify(self.output))
            self.file.write('\n')
        else:
            self.rows[key]=cnst

    def append_once(self, tag, rhs, type, *terms):
        if self.seen.get(tag):
            pass
        else:
            self.seen[tag]=True
            self.append(tag, rhs, type, *terms)

    def append(self, tag, rhs, type, *terms):
        cnst = Constraint(tag, rhs, type, terms)
        self.write_or_save(tag,cnst)

    def loadINE(self, tag, column_labels, rows):
        count=0
        for row in rows:
            count=count+1
            row_tag=tag.format(count)
            self.write_or_save(row_tag,
                Constraint(row_tag, -1*row[0],'<=',
                           zip(row[1:],column_labels)))

    def load_op(self, op, tag, column_labels):
        self.loadINE(tag, column_labels, op.inequalities)
