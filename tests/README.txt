

Each test consists of 

foo.t/spk   high level pseudocode


foo.t/bar.param  this is needed to translate the asm to an LP, using the stage2 
	       compiler. bar is just some identifier for this set of parameters;
	       typically the word size; maybe combined with input size etc..


foo.t/bar.dat/*  each .dat file conceptually describes an input for the program.
	       pragmatically it is a set of parameter settings for the LP.

